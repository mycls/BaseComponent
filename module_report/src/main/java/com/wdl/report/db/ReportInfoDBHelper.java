package com.wdl.report.db;

import com.wdl.report.app.DBManager;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class ReportInfoDBHelper {
    public static String genReportCode(){
        return UUID.randomUUID().toString().replace("-","");
    }


    //增加一条记录
    public static void upRecord(ReportInfo info){
        ReportInfoDao reportInfoDao = DBManager.getInstance().getReportDao();

        info.setReportTime(new Date());

        //先查询记录是否存在
        ReportInfo unique = reportInfoDao.queryBuilder().where(ReportInfoDao.Properties.Code.eq(info.getCode())).unique();
        if (unique==null){
            reportInfoDao.insert(info);
        }else {
            info.setId(unique.getId());
            reportInfoDao.update(info);
        }
    }


    //搜索查询记录（分页）
    public static List<ReportInfo> searchList(Integer type,String name, Date start, Date end, Integer offset, Integer limit){
        ReportInfoDao reportDao = DBManager.getInstance().getReportDao();

        QueryBuilder<ReportInfo> queryBuilder = reportDao.queryBuilder();

        if (type!=null){
            queryBuilder.where(ReportInfoDao.Properties.ReportType.eq(type));
        }

        if (name!=null&&name.length()>0){
            queryBuilder.where(ReportInfoDao.Properties.Name.like("%"+name+"%"));
        }

        if (start!=null&&end!=null){
            queryBuilder.where(ReportInfoDao.Properties.ReportTime.between(start,end));
        }

        queryBuilder.orderDesc(ReportInfoDao.Properties.ReportTime);

        queryBuilder.offset(offset*limit).limit(limit);

        return queryBuilder.list();
    }
}
