package com.wdl.report.db;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class ReportInfo {
    @Id
    private Long id;

    private String code;
    private String name;
    private String desc;
    private String age;
    private String sex; //1男 2女
    private Date reportTime;
    private String pdfPath;
    private Integer reportType;


    @Generated(hash = 1849999264)
    public ReportInfo(Long id, String code, String name, String desc, String age,
            String sex, Date reportTime, String pdfPath, Integer reportType) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.desc = desc;
        this.age = age;
        this.sex = sex;
        this.reportTime = reportTime;
        this.pdfPath = pdfPath;
        this.reportType = reportType;
    }

    @Generated(hash = 1519617947)
    public ReportInfo() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPdfPath() {
        return pdfPath;
    }

    public void setPdfPath(String pdfPath) {
        this.pdfPath = pdfPath;
    }

    public Integer getReportType() {
        return reportType;
    }

    public void setReportType(Integer reportType) {
        this.reportType = reportType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getReportTime() {
        return reportTime;
    }

    public void setReportTime(Date reportTime) {
        this.reportTime = reportTime;
    }
}
