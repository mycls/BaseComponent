package com.wdl.report.business.search.mvp.ui.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.wdl.report.R;
import com.wdl.report.db.ReportInfo;

import java.text.SimpleDateFormat;
import java.util.List;

public class ReprotListAdapter extends BaseQuickAdapter<ReportInfo, BaseViewHolder> {

    public ReprotListAdapter(int layoutResId, @Nullable List<ReportInfo> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, ReportInfo item) {
        helper.setText(R.id.p_name, isEmpty(item.getName()) ? "姓名未填" : item.getName());
        helper.setText(R.id.p_age, isEmpty(item.getAge()) ? "年龄未填" : item.getAge());
        helper.setText(R.id.p_sex, isEmpty(item.getSex()) ? "性别未填" : item.getSex());
        helper.setText(R.id.p_type, getType(item.getReportType()));

        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH点mm分ss秒 E");
        helper.setText(R.id.p_date, format.format(item.getReportTime()));
    }

    private boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    private String getType(int reportType){
        if (reportType==1){
            return "PP";
        }else if (reportType==2){
            return "PPA";
        }else if (reportType==3){
            return "PPV";
        }
        return "";
    }
}
