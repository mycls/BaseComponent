package com.wdl.report.business.ppv.mvp.presenter;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.print.PrintHelper;
import android.view.View;
import android.widget.ScrollView;

import com.github.mikephil.charting.data.Entry;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import javax.inject.Inject;

import com.wdl.report.app.manager.ReportOptManager;
import com.wdl.report.app.manager.cbt.ClassicBtManager;
import com.wdl.report.app.oss.OssManager;
import com.wdl.report.app.utils.HandCmdUtils;
import com.wdl.report.app.utils.HexUtil;
import com.wdl.report.business.ppv.mvp.contract.ReportPpvContract;
import com.wdl.report.db.ReportInfo;
import com.wdl.report.db.ReportInfoDBHelper;

import java.util.ArrayList;
import java.util.List;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 03/01/2019 21:30
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@ActivityScope
public class ReportPpvPresenter extends BasePresenter<ReportPpvContract.Model, ReportPpvContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;

    private String reportCode="ppv";

    @Inject
    public ReportPpvPresenter(ReportPpvContract.Model model, ReportPpvContract.View rootView) {
        super(model, rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }

    public void print(Context context, View... views) {
        mRootView.showLoading();


         ReportOptManager.savePdf(reportCode, views).subscribe(new Observer<String>() {
             @Override
             public void onSubscribe(Disposable d) {

             }

             @Override
             public void onNext(String savePdf) {
                 ReportOptManager.printPdf(context,savePdf);

                 mRootView.hideLoading();

                 save2DB(savePdf);
             }

             @Override
             public void onError(Throwable e) {
                 mRootView.hideLoading();


             }

             @Override
             public void onComplete() {
                 mRootView.hideLoading();
             }
         });;


    }

    public void upload(Context context,  View... views) {
        mRootView.showLoading();

        ReportOptManager.savePdf(reportCode, views).subscribe(new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(String savePdf) {
                String upload = OssManager.getInstance().upload(context, savePdf);
                mRootView.uploadSuccess(upload);
                mRootView.hideLoading();

                save2DB(savePdf);
            }

            @Override
            public void onError(Throwable e) {
                mRootView.hideLoading();


            }

            @Override
            public void onComplete() {
                mRootView.hideLoading();
            }
        });;

    }

    private void save2DB(String savePdf) {
        ReportInfo wrapReportInfo = mRootView.getWrapReportInfo();
        wrapReportInfo.setCode(reportCode);
        wrapReportInfo.setPdfPath(savePdf);
        ReportInfoDBHelper.upRecord(wrapReportInfo);
    }

    public void initReceiveListener() {
        ClassicBtManager.getInstance().setBluetoothReceiveLisenter(cmd -> handCmd(cmd));
    }

    public void startReceiveCmd() {
        reportCode= ReportInfoDBHelper.genReportCode();
        ClassicBtManager.getInstance().sendData("5AA503880003");
    }


    private void handCmd(String cmd) {
        ClassicBtManager.getInstance().sendAck();

        //首先判断是否结束指令
        if ("A5F108019F0D0A".equals(cmd)) {
//            resumeCbt();
            return;
        }

        //首先去除头尾
        cmd = cmd.replaceFirst("A5F1", "");
        cmd = cmd.replace("0D0A", "");

        //获取头
        String indexCmd = cmd.substring(0, 2);

        cmd = cmd.substring(2);

        //判断头操作
        int index = HexUtil.hexStr2Int(indexCmd);

        System.out.println("处理指令:" + indexCmd + "----" + cmd);

        if (index == 7) {
            mRootView.showTotalData(HandCmdUtils.getIntData(cmd));
        } else {
            mRootView.showChart(HandCmdUtils.getChartData(cmd));
        }
    }
}
