package com.wdl.report.business.pp.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.wdl.report.business.pp.di.module.ReportPpModule;
import com.wdl.report.business.pp.mvp.contract.ReportPpContract;

import com.jess.arms.di.scope.ActivityScope;
import com.wdl.report.business.pp.mvp.ui.activity.ReportPpActivity;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 02/15/2019 21:55
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@ActivityScope
@Component(modules = ReportPpModule.class, dependencies = AppComponent.class)
public interface ReportPpComponent {
    void inject(ReportPpActivity activity);

    @Component.Builder
    interface Builder {
        @BindsInstance
        ReportPpComponent.Builder view(ReportPpContract.View view);

        ReportPpComponent.Builder appComponent(AppComponent appComponent);

        ReportPpComponent build();
    }
}