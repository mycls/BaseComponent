package com.wdl.report.business.ppv.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.wdl.report.business.ppv.di.module.ReportPpvModule;
import com.wdl.report.business.ppv.mvp.contract.ReportPpvContract;

import com.jess.arms.di.scope.ActivityScope;
import com.wdl.report.business.ppv.mvp.ui.activity.ReportPpvActivity;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 03/01/2019 21:30
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@ActivityScope
@Component(modules = ReportPpvModule.class, dependencies = AppComponent.class)
public interface ReportPpvComponent {
    void inject(ReportPpvActivity activity);

    @Component.Builder
    interface Builder {
        @BindsInstance
        ReportPpvComponent.Builder view(ReportPpvContract.View view);

        ReportPpvComponent.Builder appComponent(AppComponent appComponent);

        ReportPpvComponent build();
    }
}