package com.wdl.report.business.pdfshow.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.github.barteksc.pdfviewer.PDFView;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;

import com.wdl.report.app.AppLifecyclesImpl;
import com.wdl.report.app.utils.pdf.PdfPrinter;
import com.wdl.report.business.pdfshow.di.component.DaggerPdfShowActivityComponent;
import com.wdl.report.business.pdfshow.mvp.contract.PdfShowActivityContract;
import com.wdl.report.business.pdfshow.mvp.presenter.PdfShowActivityPresenter;

import com.wdl.report.R;
import com.wdl.report.R2;


import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;
import me.wdl.component.commonsdk.core.RouterHub;

import static com.jess.arms.utils.Preconditions.checkNotNull;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 07/11/2019 16:21
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@Route(path = RouterHub.REPORT_SHOWPDFACTIVITY)
public class PdfShowActivityActivity extends BaseActivity<PdfShowActivityPresenter> implements PdfShowActivityContract.View {

    @BindView(R2.id.pdfView)
    PDFView pdfView;

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerPdfShowActivityComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .view(this)
                .build()
                .inject(this);
    }

    @OnClick(R2.id.print_dy)
    public void clickDy(View view) {
        PdfPrinter.doPdfPrint(PdfShowActivityActivity.this, AppLifecyclesImpl.showPdfUrl);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_pdf_show; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {
        pdfView.fromFile(new File(AppLifecyclesImpl.showPdfUrl)).load();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.snackbarText(message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }
}
