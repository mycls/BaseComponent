package com.wdl.report.business.pdfshow.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.wdl.report.business.pdfshow.di.module.PdfShowActivityModule;
import com.wdl.report.business.pdfshow.mvp.contract.PdfShowActivityContract;

import com.jess.arms.di.scope.ActivityScope;
import com.wdl.report.business.pdfshow.mvp.ui.activity.PdfShowActivityActivity;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 07/11/2019 16:21
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@ActivityScope
@Component(modules = PdfShowActivityModule.class, dependencies = AppComponent.class)
public interface PdfShowActivityComponent {
    void inject(PdfShowActivityActivity activity);

    @Component.Builder
    interface Builder {
        @BindsInstance
        PdfShowActivityComponent.Builder view(PdfShowActivityContract.View view);

        PdfShowActivityComponent.Builder appComponent(AppComponent appComponent);

        PdfShowActivityComponent build();
    }
}