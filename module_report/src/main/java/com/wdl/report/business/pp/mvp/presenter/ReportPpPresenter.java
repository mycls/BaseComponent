package com.wdl.report.business.pp.mvp.presenter;

import android.app.Application;
import android.content.Context;
import android.view.View;

import com.github.mikephil.charting.data.Entry;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import javax.inject.Inject;

import com.wdl.report.app.DBManager;
import com.wdl.report.app.manager.ReportOptManager;
import com.wdl.report.app.manager.cbt.ClassicBtManager;
import com.wdl.report.app.oss.OssManager;
import com.wdl.report.app.utils.HexUtil;
import com.wdl.report.business.pp.mvp.contract.ReportPpContract;
import com.wdl.report.db.ReportInfo;
import com.wdl.report.db.ReportInfoDBHelper;

import java.util.ArrayList;
import java.util.List;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 02/15/2019 21:55
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@ActivityScope
public class ReportPpPresenter extends BasePresenter<ReportPpContract.Model, ReportPpContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;

    private String reportCode="pp";

    @Inject
    public ReportPpPresenter(ReportPpContract.Model model, ReportPpContract.View rootView) {
        super(model, rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }

    public void print(Context context, View... views) {
        mRootView.showLoading();


        ReportOptManager.savePdf(reportCode, views).subscribe(new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(String savePdf) {
                mRootView.hideLoading();
                ReportOptManager.printPdf(context,savePdf);
                save2DB(savePdf);
            }

            @Override
            public void onError(Throwable e) {
                mRootView.hideLoading();


            }

            @Override
            public void onComplete() {
                mRootView.hideLoading();
            }
        });


    }

    private void save2DB(String savePdf) {
        ReportInfo wrapReportInfo = mRootView.getWrapReportInfo();
        wrapReportInfo.setCode(reportCode);
        wrapReportInfo.setPdfPath(savePdf);
        ReportInfoDBHelper.upRecord(wrapReportInfo);
    }

    public void upload(Context context, View... views) {
        mRootView.showLoading();

       ReportOptManager.savePdf(reportCode, views).subscribe(new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(String savePdf) {
                String upload = OssManager.getInstance().upload(context, savePdf);
                mRootView.uploadSuccess(upload);
                mRootView.hideLoading();

                save2DB(savePdf);
            }

            @Override
            public void onError(Throwable e) {
                mRootView.hideLoading();


            }

            @Override
            public void onComplete() {
                mRootView.hideLoading();
            }
        });

    }

    public void initReceiveListener(){
        ClassicBtManager.getInstance().setBluetoothReceiveLisenter(cmd -> handCmd(cmd));
    }
    public void startReceiveCmd() {
        reportCode= ReportInfoDBHelper.genReportCode();

        ClassicBtManager.getInstance().sendData("5AA503880002");
    }

    private void handCmd(String cmd) {
        ClassicBtManager.getInstance().sendAck();

        //首先判断是否结束指令
        if ("A5F108019F0D0A".equals(cmd)) {
            return;
        }

        //首先去除头尾
        cmd = cmd.replaceFirst("A5F1", "");
        cmd = cmd.replace("0D0A", "");

        //获取头
        String indexCmd = cmd.substring(0, 2);

        cmd = cmd.substring(2);

        //判断头操作
        int index = HexUtil.hexStr2Int(indexCmd);

        System.out.println("处理指令:" + indexCmd + "----" + cmd);

        if (index == 1) {
            mRootView.showQjxq(getChartData(cmd));
        } else if (index == 2) {
            mRootView.showKjjca(getChartData(cmd));
        } else if (index == 3) {
            mRootView.showKjjcb(getChartData(cmd));
        } else if (index == 4) {
            mRootView.showMjjc(getChartData(cmd));
        } else if (index == 5) {
            mRootView.showNljc(getChartData(cmd));
        } else if (index == 6) {
            mRootView.showHjxq(getChartData(cmd));
        } else if (index == 7) {
            mRootView.showTotalData(getIntData(cmd));
        }
    }

    private List<Entry> getChartData(String cmd) {
        List<Entry> entries = new ArrayList<>();
        int length = cmd.length();
        for (int i = 0; i < length; i += 2) {
            String substring = cmd.substring(i, i + 2);

            int num = HexUtil.hexStr2Int(substring);

            Entry entry = new Entry(i, num);
            entries.add(entry);
        }

        return entries;
    }

    private List<Integer> getIntData(String cmd) {
        List<Integer> entries = new ArrayList<>();
        int length = cmd.length();
        for (int i = 0; i < length; i += 2) {
            String substring = cmd.substring(i, i + 2);

            int num = HexUtil.hexStr2Int(substring);

            entries.add(num);
        }

        return entries;
    }
}
