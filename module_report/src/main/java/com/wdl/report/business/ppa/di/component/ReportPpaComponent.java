package com.wdl.report.business.ppa.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.wdl.report.business.ppa.di.module.ReportPpaModule;
import com.wdl.report.business.ppa.mvp.contract.ReportPpaContract;

import com.jess.arms.di.scope.ActivityScope;
import com.wdl.report.business.ppa.mvp.ui.activity.ReportPpaActivity;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 03/01/2019 21:06
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@ActivityScope
@Component(modules = ReportPpaModule.class, dependencies = AppComponent.class)
public interface ReportPpaComponent {
    void inject(ReportPpaActivity activity);

    @Component.Builder
    interface Builder {
        @BindsInstance
        ReportPpaComponent.Builder view(ReportPpaContract.View view);

        ReportPpaComponent.Builder appComponent(AppComponent appComponent);

        ReportPpaComponent build();
    }
}