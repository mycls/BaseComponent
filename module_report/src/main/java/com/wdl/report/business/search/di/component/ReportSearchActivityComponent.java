package com.wdl.report.business.search.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.wdl.report.business.search.di.module.ReportSearchActivityModule;
import com.wdl.report.business.search.mvp.contract.ReportSearchActivityContract;

import com.jess.arms.di.scope.ActivityScope;
import com.wdl.report.business.search.mvp.ui.activity.ReportSearchActivityActivity;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 07/08/2019 12:52
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@ActivityScope
@Component(modules = ReportSearchActivityModule.class, dependencies = AppComponent.class)
public interface ReportSearchActivityComponent {
    void inject(ReportSearchActivityActivity activity);

    @Component.Builder
    interface Builder {
        @BindsInstance
        ReportSearchActivityComponent.Builder view(ReportSearchActivityContract.View view);

        ReportSearchActivityComponent.Builder appComponent(AppComponent appComponent);

        ReportSearchActivityComponent build();
    }
}