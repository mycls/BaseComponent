package com.wdl.report.business.pp.mvp.contract;

import com.github.mikephil.charting.data.Entry;
import com.jess.arms.mvp.IView;
import com.jess.arms.mvp.IModel;
import com.wdl.report.db.ReportInfo;

import java.util.List;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 02/15/2019 21:55
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
public interface ReportPpContract {
    //对于经常使用的关于UI的方法可以定义到IView中,如显示隐藏进度条,和显示文字消息
    interface View extends IView {
        void uploadSuccess(String url);

        void showQjxq(List<Entry> entries);

        void showKjjca(List<Entry> entries);

        void showKjjcb(List<Entry> entries);

        void showMjjc(List<Entry> entries);

        void showNljc(List<Entry> entries);

        void showHjxq(List<Entry> entries);

        void showTotalData(List<Integer> datas);

        ReportInfo getWrapReportInfo();
    }

    //Model层定义接口,外部只需关心Model返回的数据,无需关心内部细节,即是否使用缓存
    interface Model extends IModel {

    }
}
