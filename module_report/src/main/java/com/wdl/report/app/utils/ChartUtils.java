package com.wdl.report.app.utils;

import android.graphics.Color;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;

public class ChartUtils {



    /**
     * 填充report data
     *
     * @param entries
     */
    public static void fillReportData(LineChart lineChart, List<Entry> entries) {
        LineData lineData = lineChart.getLineData();

        for (Entry entry : entries) {
            fillOneEntry(lineChart, lineData, 0, entry);
        }
    }

    private static void fillOneEntry(LineChart lineChart, LineData lineData, int dataSetIndex, Entry entry) {
        lineData.addEntry(entry, dataSetIndex);

        lineData.notifyDataChanged();

        lineChart.notifyDataSetChanged();
    }


    /**
     * 初始化空数据
     *
     * @param lineChart
     */
    public static void addLineFillData(LineChart lineChart, List<Entry> entries) {
        //初始化报告数据的dataset
        LineDataSet report_data = new LineDataSet(entries, "report data 2");
        report_data.setDrawCircles(false);
        report_data.setDrawValues(false);
        report_data.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        report_data.setColor(Color.BLACK);

        LineData lineData = lineChart.getLineData();
        lineData.addDataSet(report_data);

        lineChart.notifyDataSetChanged();
        lineChart.invalidate();
    }

    /**
     * 初始化图表相关
     *
     * @param lineChart
     */
    public static void initChart(LineChart lineChart) {
        initLineChartView(lineChart);

        initLineEmptyData(lineChart);
    }

    /**
     * 初始化通用linechart
     *
     * @param lineChart
     */
    public static void initLineChartView(LineChart lineChart) {
        //设置view
        lineChart.setNoDataText("暂无数据");// 没有数据的时候默认显示的文字
        lineChart.setNoDataTextColor(Color.GRAY);
        lineChart.setBorderColor(Color.BLUE);
        lineChart.getLegend().setEnabled(false);// 不显示图例
        lineChart.getDescription().setEnabled(false);// 不显示描述
        lineChart.setScaleEnabled(false);   // 取消缩放
        lineChart.setTouchEnabled(false);
        lineChart.setDragEnabled(false);
        //设置XY轴动画效果
        lineChart.animateY(2500);
        lineChart.animateX(1500);

        /***XY轴的设置***/
        lineChart.getAxisRight().setEnabled(false);

        XAxis xAxis = lineChart.getXAxis();
        YAxis leftYAxis = lineChart.getAxisLeft();
        leftYAxis.setXOffset(2f);
        leftYAxis.setValueFormatter((value, axis) -> {
            int IValue = (int) value;

            if (IValue % 25 == 0) {
                return String.valueOf(IValue) + "--";
            }

            return "-";
        });

        //保证Y轴从0开始，不然会上移一点
        leftYAxis.setMinWidth(0f);
        leftYAxis.setAxisMinimum(0f);
        leftYAxis.setAxisMaximum(100f);
        leftYAxis.setGranularity(5f);
        leftYAxis.setLabelCount(20, false);
        leftYAxis.setDrawGridLines(false);
        leftYAxis.setAxisLineWidth(1f);
        leftYAxis.setAxisLineColor(Color.GRAY);
        leftYAxis.setTextSize(4);


        LimitLine topLimit = new LimitLine(100f, "");
        topLimit.setLineWidth(1f);
        topLimit.setLineColor(Color.GRAY);
        leftYAxis.addLimitLine(topLimit);

        //设置x轴
        xAxis.setEnabled(false);
    }

    /**
     * 初始化空数据
     *
     * @param lineChart
     */
    public static void initLineEmptyData(LineChart lineChart) {
        //初始化报告数据的dataset
        LineDataSet report_data = new LineDataSet(null, "report data");
        report_data.setDrawCircles(false);
        report_data.setDrawValues(false);
        report_data.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        report_data.setColor(Color.BLACK);

        List<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(report_data);

        LineData lineData = new LineData(dataSets);
        lineChart.setData(lineData);
    }

    public static void resetLinechart(LineChart lineChart){
        lineChart.clear();

        initLineEmptyData(lineChart);
    }
}
