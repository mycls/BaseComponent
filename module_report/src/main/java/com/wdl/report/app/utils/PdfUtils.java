package com.wdl.report.app.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.pdf.PdfDocument;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.vondear.rxtool.RxFileTool;
import com.wdl.report.app.utils.pdf.PdfItextUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PdfUtils {
    private static final String TAG = "PdfUtils";

    private static Bitmap resizeBitmap(int planHeight, int planWidth, Bitmap bitmap) {
        int realWidth = bitmap.getWidth();
        int realHeight = bitmap.getHeight();

        double percent = realWidth / (double) planWidth;
        int showHeight = (int) ((realHeight / percent) + 1);

        showHeight = showHeight > planHeight ? planHeight : showHeight;

        return ViewUtils.scoreImg(bitmap, planWidth, showHeight);
    }

    public static void createPdfFromView(final String pdfPath, View... views) {
        if (views == null || views.length < 1 || pdfPath == null) {
            Log.e(TAG, "content and pdfPath can not be null!");
            return;
        }

        int planWidth = (int) PageSize.A4.getWidth() - 40;
        int planHeight = (int) PageSize.A4.getHeight() - 70;

        List<String> imgPath = new ArrayList<>();

        for (View content : views) {
            File file = new File(pdfPath);
            String parent = file.getParent();
            String filePath = parent + "/" + System.currentTimeMillis() + ".png";
            if (content instanceof LinearLayout) {
                Bitmap bitmap = ViewUtils.getLinearLayoutBitmap((LinearLayout) content);

//                Bitmap nowBitmap = resizeBitmap(planHeight, planWidth, bitmap);

                ViewUtils.saveBitmapAsPng(bitmap, new File(filePath));

                imgPath.add(filePath);
            } else if (content instanceof ScrollView) {
                Bitmap bitmap = ViewUtils.shotScrollView((ScrollView) content);

//                Bitmap nowBitmap = resizeBitmap(planHeight, planWidth, bitmap);

                ViewUtils.saveBitmapAsPng(bitmap, new File(filePath));

                imgPath.add(filePath);
            }
        }

        PdfItextUtil pdfItextUtil = null;
        try {
            pdfItextUtil = new PdfItextUtil(pdfPath);
            int count = 1;
            for (String filePath : imgPath) {
//                pdfItextUtil.addPngToPdf(new FileInputStream(filePath));
                if (count == 2) {
                    pdfItextUtil.addTextToPdf("");
                    pdfItextUtil.addTextToPdf("");
                }
                pdfItextUtil.addImageToPdfCenterH(filePath, planWidth, planHeight);
                count++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (pdfItextUtil != null) {
                pdfItextUtil.close();
            }

            for (String filePath : imgPath) {
                RxFileTool.delAllFile(filePath);
            }
        }
    }


    public static void createPdfFromView2(final String pdfPath, View... views) {
        if (views == null || views.length < 1 || pdfPath == null) {
            Log.e(TAG, "content and pdfPath can not be null!");
            return;
        }

        PdfDocument document = new PdfDocument();

        int measuredHeight = views[0].getMeasuredHeight() + 20;
        int measuredWidth = views[0].getMeasuredWidth() + 20;

//        int measuredHeight = 1946;
//        int measuredWidth = 1032;

        for (View content : views) {
            int height = content.getMeasuredHeight() / 4;
            int width = content.getMeasuredWidth() / 4;
            int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
            int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);
            content.measure(widthMeasureSpec, heightMeasureSpec);

            PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(
                    measuredWidth, measuredHeight, 1)
                    .create();

            PdfDocument.Page page = document.startPage(pageInfo);

            Canvas canvas = page.getCanvas();
            content.draw(canvas);

            document.finishPage(page);
        }
        FileOutputStream os = null;
        try {
            os = new FileOutputStream(pdfPath);
            document.writeTo(os);
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            document.close();
        }
    }
}
