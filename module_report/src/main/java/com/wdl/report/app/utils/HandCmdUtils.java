package com.wdl.report.app.utils;

import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;
import java.util.List;

public class HandCmdUtils {
    public static List<Entry> getChartData(String cmd) {
        List<Entry> entries = new ArrayList<>();
        int length = cmd.length();
        for (int i = 0; i < length; i += 2) {
            String substring = cmd.substring(i, i + 2);

            int num = HexUtil.hexStr2Int(substring);

            Entry entry = new Entry(i, num);
            entries.add(entry);
        }

        return entries;
    }

    public static List<Entry> getChartDataExZero(String cmd) {
        List<Entry> entries = new ArrayList<>();
        List<Entry> entriesLast = new ArrayList<>();
        int length = cmd.length();
        int emtCount = 0;
        for (int i = 0; i < length; i += 2) {
            String substring = cmd.substring(i, i + 2);

            int num = HexUtil.hexStr2Int(substring);


            Entry entry = new Entry(i, num);
            if (num == 0) {
                entriesLast.add(entry);
                emtCount++;
            } else {
                if (i >= length - 11) {
                    if (emtCount > 50) {
                        break;
                    }
                }
                entries.addAll(entriesLast);
                entries.add(entry);
                entriesLast.clear();
                emtCount = 0;
            }
        }

        return entries;
    }

    public static List<Integer> getIntData(String cmd) {
        List<Integer> entries = new ArrayList<>();
        int length = cmd.length();
        for (int i = 0; i < length; i += 2) {
            String substring = cmd.substring(i, i + 2);

            int num = HexUtil.hexStr2Int(substring);

            entries.add(num);
        }

        return entries;
    }
}
