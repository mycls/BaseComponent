package com.wdl.report.app.manager;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.vondear.rxtool.RxFileTool;
import com.vondear.rxtool.RxImageTool;
import com.wdl.report.app.oss.OssManager;
import com.wdl.report.app.utils.PdfUtils;
import com.wdl.report.app.utils.ViewUtils;
import com.wdl.report.app.utils.pdf.PdfPrinter;

import java.io.File;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import top.zibin.luban.Luban;

public class ReportOptManager {
    public static Observable<String> shotAndUploadView(final Context context, ScrollView view, String signName) {
        final String srcPath = RxFileTool.getSDCardPath() + "report_tmp/" + signName + ".jpg";

        return Observable.just(view)
                .map(view1 -> {
                    Bitmap bitmap = ViewUtils.shotScrollView(view1);

                    //截屏保存
                    RxImageTool.save(bitmap, srcPath, Bitmap.CompressFormat.JPEG, true);

                    return srcPath;
                })
                .map(s -> {
                    List<File> files = Luban.with(context).load(Collections.singletonList(s)).get();

                    return files != null && files.size() > 0 ? files.get(0).getAbsolutePath() : null;
                })
                .map(filePath -> {
                    if (filePath == null) {
                        return null;
                    }
                    return OssManager.getInstance().upload(context, filePath);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<Bitmap> shotAndPrintView(ScrollView view) {
        return Observable.just(view)
                .map(ViewUtils::shotScrollView)
                .map(bitmap -> ViewUtils.scoreImg(bitmap, 7070, 10000))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<Bitmap> shotAndPrintViewHalfA4(ScrollView view) {
        return Observable.just(view)
                .map(ViewUtils::shotScrollView)
                .map(bitmap -> {
                    Bitmap resizeImage = ViewUtils.resizeImage(bitmap, 1190, 842);
                    Bitmap newBit = Bitmap.createBitmap(1190, 1684, Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(newBit);
                    canvas.drawBitmap(resizeImage, 0, 0, null);
                    return newBit;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


    //存储pdf
    public static Observable<String> savePdf(String code, View... views) {
        return Observable.just(views)
                .map((View[] views1) -> {
                    String srcPath = RxFileTool.getSDCardPath() + "report_app/pdf/";
                    File file = new File(srcPath);
                    if (!file.exists()) {
                        file.mkdirs();
                    }

                    String filePath = srcPath + code + ".pdf";

                    PdfUtils.createPdfFromView(filePath, views1);

                    return filePath;
                })
                .map(filePath -> filePath)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    //打印pdf
    public static void printPdf(Context context, String pdfPath) {
        PdfPrinter.doPdfPrint(context, pdfPath);
    }
}
