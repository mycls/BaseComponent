package com.wdl.report.app.utils.pdf;

import android.content.Context;
import android.os.Build;
import android.print.PrintManager;

import androidx.annotation.RequiresApi;

public class PdfPrinter {

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void doPdfPrint(Context context, String filePath) {
        PrintManager printManager = (PrintManager) context.getSystemService(Context.PRINT_SERVICE);
        MyPrintPdfAdapter myPrintAdapter = new MyPrintPdfAdapter(filePath);
        printManager.print("jobName", myPrintAdapter, null);
    }
}
