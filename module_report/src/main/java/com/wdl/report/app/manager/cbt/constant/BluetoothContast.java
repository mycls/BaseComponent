package com.wdl.report.app.manager.cbt.constant;

import java.util.UUID;

public class BluetoothContast {
    public static final UUID BLUETOOTH_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    public static String BLUETOOTH_NAME = "HC-06";
}
