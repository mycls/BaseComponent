package com.wdl.report.app;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.wdl.report.db.DaoMaster;
import com.wdl.report.db.DaoSession;
import com.wdl.report.db.ReportInfoDao;

public class DBManager {
    private static final String DATA_BASE_NAME="bj-report-db";

    private DaoSession mDaoSession;

    private static DBManager instance;

    private DBManager(){

    }

    public static DBManager getInstance(){
        if (instance==null){
            instance=new DBManager();
        }

        return instance;
    }

    public void init(Context context){
        DaoMaster.DevOpenHelper mHelper=new DaoMaster.DevOpenHelper(context,DATA_BASE_NAME,null);
        SQLiteDatabase  db=mHelper.getWritableDatabase();
        DaoMaster    mDaoMaster=new DaoMaster(db);
        mDaoSession=mDaoMaster.newSession();
    }

    public ReportInfoDao getReportDao(){
      return   mDaoSession.getReportInfoDao();
    }
}
