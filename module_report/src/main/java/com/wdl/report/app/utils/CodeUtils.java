package com.wdl.report.app.utils;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.vondear.rxtool.view.RxToast;

public class CodeUtils {
    public static void showErCode(ImageView iv, String content) {
        try {
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.encodeBitmap(content, BarcodeFormat.QR_CODE, 400, 400);
            iv.setImageBitmap(bitmap);
        } catch (Exception e) {
            RxToast.normal("打印异常，稍后重试");
        }
    }
}
