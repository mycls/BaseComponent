package com.wdl.ble.app.utils;

public class BlueToothUtils {

    //byte数组转化为16进制hex
    final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static byte[] hexStringToByte(String hex) {
        int len = (hex.length() / 2);
        byte[] result = new byte[len];
        char[] achar = hex.toCharArray();
        for (int i = 0; i < len; i++) {
            int pos = i * 2;
            result[i] = (byte) (toByte(achar[pos]) << 4 | toByte(achar[pos + 1]));
        }
        return result;
    }

    private static int toByte(char c) {
        byte b = (byte) "0123456789ABCDEF".indexOf(c);
        return b;
    }

    /**
     * 根据测试说明：机器周期性发送压力值数据指令（A5 F1 03 00 00 00）
     * 目前只需要用到第4位数据即蓝色那位。 其值的大小，会根据按压气囊的值而相应改变。
     * 所以需要得到第四部分，转化为10进制，然后在改变高度
     * 转化为10进制的压力值,最大值120左右
     *
     * @param value
     * @return -1 为数据接收错误
     */
    public static int getMachinePressureNum(byte[] value) {
        String strValue = bytesToHex(value);

        if (strValue.length() < 8) {
            return -1;
        }

        String the4Num = strValue.substring(6, 8);

        return Integer.parseInt(the4Num, 16);
    }


}
