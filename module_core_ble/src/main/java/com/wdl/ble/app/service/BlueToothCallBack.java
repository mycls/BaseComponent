package com.wdl.ble.app.service;

import com.vise.baseble.core.BluetoothGattChannel;
import com.vise.baseble.core.DeviceMirror;
import com.vise.baseble.model.BluetoothLeDevice;

public interface BlueToothCallBack {
    void connectResult(boolean isSuccess, boolean isDisconnected, DeviceMirror deviceMirror);

    void onNotifyData(BluetoothLeDevice bluetoothLeDevice, BluetoothGattChannel bluetoothGattChannel,byte[] data);

    void onCallbackData(boolean isSuccess, BluetoothLeDevice bluetoothLeDevice,BluetoothGattChannel bluetoothGattChannel,byte[] data);

}
