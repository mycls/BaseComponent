package com.wdl.ble.app.base;

import com.jess.arms.mvp.IPresenter;

public interface IBlePresenter extends IPresenter {
    void bleConnete();
}
