package com.wdl.ble.app.base;

import com.jess.arms.mvp.IView;

public interface IBleView extends IView {
    void bledataChanged(int process);
}
